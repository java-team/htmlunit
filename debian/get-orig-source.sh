#!/bin/sh

PACKAGE=htmlunit
SRC_VERSION=$2
TARBALL=../${PACKAGE}_${SRC_VERSION}.orig.tar.xz

rm -rf get-orig-source $TARBALL $3
mkdir get-orig-source
svn export https://htmlunit.svn.sourceforge.net/svnroot/htmlunit/tags/HtmlUnit-${SRC_VERSION} \
    get-orig-source/${PACKAGE}-${SRC_VERSION}
# remove CVS metadata
find get-orig-source/ -depth -name CVS -exec rm -r {} \;
find get-orig-source/ -name .cvsignore -exec rm  {} \;
# remove binary libraries
find get-orig-source/${PACKAGE}-${SRC_VERSION}/ -name "*.jar" -exec rm {} \;
# remove non DFSG-compatible but not needed files
rm -rf get-orig-source/${PACKAGE}-${SRC_VERSION}/cruise/
XZ_OPT=--best tar cJf $TARBALL -C get-orig-source ${PACKAGE}-${SRC_VERSION}
rm -rf get-orig-source
echo "  "$TARBALL" created"
